import './style.css'

import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000);

const renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector("#bg"),
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth,window.innerHeight);
camera.position.setZ(30);

renderer.render(scene,camera);

// Floating Donut
const geometry = new THREE.TorusGeometry(10,3,16,100)
const material = new THREE.MeshStandardMaterial({color:0xAABB00});
const torus = new THREE.Mesh(geometry, material);

// Scene composition 
scene.add(torus)

// Lights
const pointLight = new THREE.PointLight(0XFFFFFF)
pointLight.position.set(5, 5, 5)

const ambientLight = new THREE.AmbientLight(0xFFFFFF);
scene.add(pointLight, ambientLight)

// Helpers
const lightHelper = new THREE.PointLightHelper(pointLight)
const gridHelper = new THREE.GridHelper(200, 50);
const axesHelper = new THREE.AxesHelper(5);

scene.add(lightHelper, gridHelper, axesHelper)

// Controls (mouse pan rotate and zoom)
const controls = new OrbitControls(camera, renderer.domElement);

// Randomly generated objects
function addStar(){
  const geometry = new THREE.SphereGeometry(.25, 24, 24)
  const material = new THREE.MeshStandardMaterial({color:0xFFFFFF})
  const star = new THREE.Mesh(geometry,material)

  const [x, y, z] = Array(3).fill().map(() => THREE.MathUtils.randFloatSpread(100))

  star.position.set(x, y, z)
  scene.add(star)
}

// Create array and fill with addStar function
Array(200).fill().forEach(addStar)

// Background image
const spaceTexture = new THREE.TextureLoader().load('images/space.jpg')
scene.background = spaceTexture

// loop function (to be called and repeated indefinitely)
function animate(){
  requestAnimationFrame(animate);

  torus.rotation.x += .01;
  torus.rotation.y += .005;
  torus.rotation.z += .01;

  renderer.render(scene,camera);
}

animate()

